const { app, BrowserWindow } = require('electron');

// require('electron-reload')(__dirname);
 
function createWindow () {
  // Create the browser window.
  let win = new BrowserWindow({
    width: 1300, height: 720,
    minWidth: 1300, minHeight: 720,
    webPreferences: {
      nodeIntegration: true
    }
  });
 
  // and load the index.html of the app.
  win.loadFile('index.html');
}
app.on('ready', createWindow);