import React, { Component } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import ReactTable, { Column } from 'react-table';
import { random2 } from '../libraries/random-x';
import CtxMenu from '../components/CtxMenu';

interface Props { }

interface States {
  data: Data[]
}

//#region Interfaces
interface Data {
  id: number;
  orderPlaced: Date;
  itemCount: number;
  itemCost: number;
  expectedDate: Date;
}
//#endregion

class VendorPage extends Component<Props, States> {
  constructor(props: Props) {
    super(props);

    let data = new Array<Data>(20);

    for (let i = 0; i < data.length; i++) {
      data[i] = {
        id: i,
        orderPlaced: new Date(),
        itemCount: random2(10),
        itemCost: Math.random() * 30,
        expectedDate: new Date().addDays(random2(10))
      }
    }

    this.state = {
      data: data
    };
  }

  dataTable = () => {
    const { data } = this.state;
    let dateFormat = 'ddd dd, MMM, yyyy, hh:mm tt';
    return (
      <table className="table table-hover table-bordered border w-100 my-0">
        <thead>
          <tr className="text-center bg-dark text-white">
            <th>#</th>
            <th>Order Placed On</th>
            <th>Expected date</th>
            <th>Amount</th>
            <th className="text-right">Total Cost</th>
          </tr>
        </thead>
        <tbody>
          {data.map((x, i) => (
            <tr key={i} className="text-center">
              <td>{x.id + 1}</td>
              <td>
                <div className="btn-link pl-2">
                  <span className="selectable">{x.orderPlaced.toString(dateFormat)}</span>
                </div>
              </td>
              <td>{x.orderPlaced.addDays(random2(10)).toString('ddd dd, MMM, yyyy')}</td>
              <td>{x.itemCount}</td>
              <td className="text-right">${(x.itemCost * x.itemCount).toFixed(2)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    )
  }

  render() {
    return (
      <Scrollbars className="w-100 h-100">
        <div className="p-4 d-flex flex-column w-100">
          {/* Column 1 */}
          <div className=" w-100 d-flex flex-row justify-content-center align-items-center">
            <div className="bg-white w-100">
              <this.dataTable />
            </div>
          </div>
        </div>
        <div className="w-100" style={{ position: 'sticky', bottom: 0 }}>
          <div className="text-center bg-light shadow py-2">
            <button className="btn btn-primary">Filter</button>
          </div>
        </div>
      </Scrollbars>
    );
  }
}

export default VendorPage;