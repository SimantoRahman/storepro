import React, { Component, CSSProperties } from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import { Doughnut } from 'react-chartjs-2';
import ViewInventory from '../components/Inventory/ViewInventory';
import CtxMenu from '../components/CtxMenu';
import Add from '../components/Inventory/Add';
import { NavContext } from '../App';
import {BarChart, LineChart} from '../components/chart-js';

//#region Interfaces

interface Props {
  showToolbar: boolean,
  onNavChange?: (navLink?: string) => void
};

interface State {
  lineChartData: LineChartProps;
  barChartData: LineChartProps;
  hoverElement?: JSX.Element;
};

interface ChartOptions {
  title?: {
    display: boolean,
    fontSize?: number,
    text: string
  },
  animation?: boolean,
  responsive?: boolean,
  height?: number,
  legend?: {
    position?: string
  },
  tooltips?: {
    mode: 'label' | null
  },
  maintainAspectRatio?: boolean
};

interface LineChartProps {
  labels: Array<string>,
  datasets: Array<{
    label: string,
    data: Array<number>
    backgroundColor?: string | Array<string>,
    borderColor?: string,
    pointRadius?: number,
    pointBackgroundColor?: string,
    lineTension?: number,
    borderWidth?: number,
  }>
};

//#endregion

class InventoryPage extends Component<Props, State> {
  public static defaultProps: Props = {
    showToolbar: false
  };

  getChartOptions = (
    props: { title?: string, height?: number, legendPos?: 'top' | 'bottom' | 'left' | 'right' }) => {
    let options: ChartOptions = {
      title: props.title && {
        display: true,
        fontSize: 24,
        text: props.title
      },
      legend: {
        position: props.legendPos ? props.legendPos : "top"
      },
      tooltips: {
        mode: "label"
      },
      maintainAspectRatio: true,
      responsive: true,
      height: props.height ? props.height : ''
    } as ChartOptions;

    return options
  }

  UNSAFE_componentWillMount() {
    this.state = {
      lineChartData: this.fetchData("hollow"),
      barChartData: this.fetchData("solid")
    }
  }

  viewInventory = () => {
    let inventory = (
      <ViewInventory onNavChange={this.props.onNavChange!} onClose={() => this.setState({ hoverElement: undefined })} />
    );

    this.setState({
      hoverElement: inventory
    })
  }

  //#region Functions
  getColorPalette = (type: "solid" | "hollow", n: number = 1) => {
    const colorPalettes = [
      { solid: "#DEB887", transparent: 'rgba(222, 184, 135, 0.3)' },
      { solid: "#A9A9A9", transparent: 'rgba(169, 169, 169, 0.3)' },
      { solid: "#DC143C", transparent: 'rgba(220, 20, 60, 0.3)' },
      { solid: "#F4A460", transparent: 'rgba(244, 164, 96, 0.3)' }
    ]

    let random = (limit: number) => {
      let n = Math.round(Math.random() * limit);
      console.log(n);
      return n;
    }

    return colorPalettes[random(3)];
  }

  fetchData = (type: "solid" | "hollow") => {

    const setA = this.getColorPalette(type);
    const setB = this.getColorPalette(type);

    return {
      labels: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
      datasets: [
        {
          label: "2018",
          backgroundColor: setA.solid,
          borderColor: setA.solid,
          pointRadius: 4,
          pointBackgroundColor: "rgba(0, 0, 0, 0.5)",
          lineTension: 0.4,
          borderWidth: 2,
          data: Array.from({ length: 12 }, () => parseFloat((Math.random() * 20).toFixed(2)))
        },
        {
          label: "2019",
          backgroundColor: setB.solid,
          borderColor: setB.solid,
          pointRadius: 4,
          pointBackgroundColor: "rgba(0, 0, 0, 0.5)",
          lineTension: 0.4,
          borderWidth: 2,
          data: Array.from({ length: 12 }, () => parseFloat((Math.random() * 20).toFixed(2)))
        }
      ]
    }
  }

  setData = () => {
    this.setState({
      lineChartData: {
        labels: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
        datasets: [
          {
            label: "2018",
            // backgroundColor: "rgba(176, 48, 48, 0.44)",
            borderColor: "rgba(176, 48, 48)",
            pointRadius: 4,
            pointBackgroundColor: "rgba(0, 0, 0, 0.5)",
            lineTension: 0.4,
            borderWidth: 2,
            data: Array.from({ length: 12 }, () => parseFloat((Math.random() * 20).toFixed(2)))
          },
          {
            label: "2019",
            // backgroundColor: 'rgba(92, 213, 102, 0.44)',
            borderColor: '#3eb865',
            pointRadius: 4,
            pointBackgroundColor: "rgba(0, 0, 0, 0.5)",
            lineTension: 0.4,
            borderWidth: 2,
            data: Array.from({ length: 12 }, () => parseFloat((Math.random() * 20).toFixed(2)))
          }
        ]
      }
    });
  };

  removeData(datasetIndex: number, _index: number) {
    console.log(`Dataset Index: ${datasetIndex}, Data Index: ${_index}`);
  };
  //#endregion

  render() {
    const { lineChartData, barChartData, hoverElement } = this.state;

    return (
      <div className="w-100 h-100 d-flex flex-column position-relative">
        <NavContext.Consumer>
          {({ setNav }) => (
            <Switch>
              <Route path="/inventory/add">
                <Add onNavChange={setNav!} />
              </Route>
              <Route path="/inventory/order/add">
                <ViewInventory onNavChange={setNav!} />
              </Route>
              <Route>
                {hoverElement && (
                  <div className="position-absolute top-0 d-flex justify-content-center align-items-center w-100 h-100">
                    {hoverElement}
                  </div>
                )}
                <Scrollbars className="m-0" autoHide autoHideTimeout={200} autoHideDuration={500}>
                  <div className='text-dark p-4'>
                    <div className='d-flex flex-column w-100 shadow'>
                      {/* Row 1 */}
                      <div className='card-group w-100 rounded-0'>
                        <div className='card rounded-0 font-cabin'>
                          <big className="card-header">In Stock</big>
                          <div className="card-body">
                            <big><i className="fas fa-boxes" /> 40,228</big>
                          </div>
                        </div>
                        <div className="card rounded-0 font-cabin">
                          <big className="card-header">Backup Status</big>
                          <div className="card-body">
                            <big>
                              <i className="fas fa-cloud-upload-alt" /> <span className="badge badge-success">online</span>
                            </big>
                          </div>
                        </div>
                        <div className="card rounded-0 font-cabin">
                          <big className="card-header">Active Orders</big>
                          <div className="card-body">
                            <big><i className="fas fa-truck" /> 08</big>
                          </div>
                        </div>
                        <div className="card rounded-0 font-cabin">
                          <big className="card-header">Next Delivery</big>
                          <div className="card-body ">
                            <big><i className="fas fa-calendar-check" /> Mon, June 20, 2019</big>
                          </div>
                        </div>
                      </div>
                      {/* Row 2 */}
                      <div className="d-flex border border-top-0">
                        <div className="w-50 h-auto">
                          <div className="bg-white p-2 rounded-0">
                            <div className="d-flex w-100">
                              <h5 className="font-cabin">Current Stock</h5>
                              <select className="ml-auto" defaultValue={2}>
                                <option value={0}>By Week</option>
                                <option value={1}>By Month</option>
                                <option value={2}>By Year</option>
                              </select>
                            </div>
                            <LineChart className="w-100 h-100"
                              contextMenu={[
                                { label: "Settings", icon: "fas fa-cogs" },
                                { label: "Update", icon: "fas fa-sync", onClick: this.setData }
                              ]}
                              data={{
                                labels: ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
                                datasets: [{
                                  label: '2019',
                                  data: [20, 18, 21, 22, 20, 18, 21, 22, 20, 18, 21, 22]
                                }, {
                                  label: '2018',
                                  data: [19, 16, 20, 20, 19, 16, 20, 20, 19, 16, 20, 20]
                                }]
                              }}
                              closeOnClick
                            />
                          </div>
                        </div>
                        <div className="w-50 h-auto">
                          <div className="bg-white p-2 border-left rounded-0">
                            <div className="d-flex w-100">
                              <h5 className="font-cabin">Turnover Rate</h5>
                              <select className="ml-auto" defaultValue={2}>
                                <option value={0}>By Week</option>
                                <option value={1}>By Month</option>
                                <option value={2}>By Year</option>
                              </select>
                            </div>
                            <BarChart className="w-100 h-100"
                              contextMenu={[
                                { label: "Settings", icon: "fas fa-cogs" },
                                { label: "Update", icon: "fas fa-sync", onClick: this.setData }
                              ]}
                              data={{
                                labels: ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
                                datasets: [{
                                  label: '2019',
                                  data: [20, 18, 21, 22, 20, 18, 21, 22, 20, 18, 21, 22]
                                }, {
                                  label: '2018',
                                  data: [19, 16, 20, 20, 19, 16, 20, 20, 19, 16, 20, 20]
                                }]
                              }}
                              closeOnClick
                            />
                          </div>
                        </div>
                      </div>
                      {/* Row 3 */}
                      <div className="d-flex border border-top-0">
                        <div className="d-flex flex-column border-right p-3 bg-white w-75">
                          <b className="text-uppercase">Product Details</b>
                          <ul className="list-group">
                            <li className="list-group-item d-flex justify-content-between align-items-center">
                              Low Stock Items
                            <span className="badge badge-info badge-pill">4</span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center">
                              Group Count
                            <span className="badge badge-info badge-pill">18</span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center">
                              Morbi leo risus
                            <span className="badge badge-info badge-pill">1</span>
                            </li>
                          </ul>
                          <div className="pt-2 text-center">
                            <div className="d-flex flex-row justify-content-around">
                              <button className="btn btn-sm btn-primary" onClick={this.viewInventory}>
                                Inventory <i className="fas fa-arrow-right" />
                              </button>
                              <button className="btn btn-sm btn-primary">Forecasts</button>
                              <button className="btn btn-sm btn-primary">Obsolete</button>
                            </div>
                          </div>
                        </div>
                        <div className="d-flex flex-column border-right p-3 bg-white w-50">
                          <CtxMenu className="w-100 m-auto"
                            closeOnClick={true}
                            items={[
                              { label: "Reload", icon: "fas fa-sync" }
                            ]}>
                            <Doughnut
                              options={this.getChartOptions({ legendPos: "left" })}
                              data={{
                                labels: ["Gatorates", "Monsters", "Chips", "Beer"],
                                datasets: [
                                  {
                                    data: [12, 14, 18, 23],
                                    backgroundColor: [
                                      "#DEB887",
                                      "#A9A9A9",
                                      "#DC143C",
                                      "#F4A460"
                                    ]
                                  }
                                ]
                              } as LineChartProps}
                            />
                          </CtxMenu>
                        </div>
                        <div className="d-flex flex-column p-3 bg-white w-50">
                          <div className="w-100 m-auto">
                            <Doughnut
                              options={this.getChartOptions({ legendPos: "left" })}
                              data={{
                                labels: ["Gatorates", "Monsters", "Chips", "Beer"],
                                datasets: [
                                  {
                                    data: [12, 14, 18, 23],
                                    backgroundColor: [
                                      "#DEB887",
                                      "#A9A9A9",
                                      "#DC143C",
                                      "#F4A460"
                                    ]
                                  }
                                ]
                              } as LineChartProps}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Scrollbars>
              </Route>
            </Switch>
          )}
        </NavContext.Consumer>
      </div>
    );
  }
}

export default InventoryPage;