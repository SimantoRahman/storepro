import React from 'react';
import ReactLoading from 'react-loading';
import { NavLink, Switch, Route, Redirect } from 'react-router-dom';
import InventoryPage from './pages/InventoryPage';
import Sidebar from './components/Sidebar';
// @ts-ignore
import Clock from 'react-live-clock';
import 'bootstrap/dist/css/bootstrap.min.css';
import './resources/animate.css';
import './App.css';
import VendorPage from './pages/VendorPage';

//#region Interfaces
interface IClockProps {
  ago: boolean,
  children: string,
  className: string,
  date: number | string,
  format: string,
  from: string,
  fromNow: boolean,
  interval: number,
  locale: string,
  parse: string,
  to: string,
  toNow: boolean,
  unix: boolean,
  ticking: boolean,
  timezone: string,
  filter: Function,
  onChange: Function
};

interface IUser {
  username: string,
  accessMode: number
}

interface State {
  user?: IUser;
  isLoaded: boolean;
  scale: number;
  contextMenu?: {
    element: JSX.Element,
    x: number,
    y: number
  };
  sidebar: boolean;
  navback?: string;
}

export interface Conf {
  prefCurrency: 'USD' | 'CAD' | 'EUR',
  theme: 'light',
  lastLoc: string | null
}

type NavContextProps = {
  setNav: (navLink?: string) => void
}

export const NavContext = React.createContext<Partial<NavContextProps>>({})
//#endregion

class App extends React.Component<{}, State> {

  constructor(props: {}) {
    super(props);
    this.state = {
      user: { username: "SimantoR", accessMode: 0 },
      isLoaded: true,
      scale: 2.0,
      sidebar: false
    };
  }

  componentDidMount() {
    this.defaults();
  }

  defaults = () => {
    const store = localStorage;

    if (!store.getItem('_conf')) {
      localStorage.setItem("_conf", JSON.stringify({
        prefCurrency: 'USD',
        theme: 'light',
        lastLoc: null
      } as Conf));
    }

    if (!store.getItem('_appconf')) {
      store.setItem('_appconf', JSON.stringify(null));
    }
  }

  // set context menu
  setMenu = (node: JSX.Element, x: number, y: number) => {
    this.setState({ contextMenu: { element: node, x: x, y: y } });
  }

  setNavBack = (navLink?: string) => this.setState({ navback: navLink });

  navBar = () => {
    return (
      <div className="d-flex flex-row" style={{
        background: "linear-gradient(180deg, #91bbff, #6692d9)",
        borderTop: "1px solid #6692d9"
      }}>
        <div className="btn-group btn-group-lg mx-auto">
          <NavLink exact to="/" className="btn btn-lg btn-outline-dark rounded-0">
            <i className="far fa-chart-bar" />
          </NavLink>
          <NavLink to="/server" className="btn btn-lg btn-outline-dark">
            <i className="fas fa-server" />
          </NavLink>
          <NavLink to="/cloud" className="btn btn-lg btn-outline-dark">
            <i className="fas fa-cloud-download-alt" />
          </NavLink>
          <NavLink to="/bitcoin" className="btn btn-lg btn-outline-dark">
            <i className="fab fa-bitcoin" />
          </NavLink>
          <NavLink to="/connection" className="btn btn-lg btn-outline-dark">
            <i className="fas fa-satellite-dish" />
          </NavLink>
          <NavLink to="/inventory" className="btn btn-lg btn-outline-dark rounded-0">
            <i className="fas fa-tags" />
          </NavLink>
        </div>
      </div>
    )
  }

  topBar = () => {
    return (
      <div className="w-100 d-flex flex-row bg-light border-bottom text-dark px-2">
        {this.state.navback && (
          <div className="btn-group btn-group-sm my-auto">
            <NavLink exact to={this.state.navback} className="btn btn-sm btn-block no-outline"
              style={{ fontSize: "22px" }}>
              <i className="fas fa-chevron-circle-left no-outline"
                style={{ transition: 'all 0.2s' }}
                onMouseEnter={(e) => {
                  let { style } = e.currentTarget;
                  style.textShadow = "-2px -2px 5px black";
                  style.color = "white";
                  style.transform = "scale(1.3)"
                }}
                onMouseLeave={(e) => {
                  let { style } = e.currentTarget;
                  style.textShadow = "";
                  style.color = "black";
                  style.transform = "";
                }} />
            </NavLink>
          </div>
        )}
        
        {/* Search bar */}
        {/* <form className="form-group my-auto bg-white rounded-pill border">
          <div className="input-group">
            <input type="search" className="form-control bg-transparent border-0" />
            <div className="input-group-append">
              <div className="input-group-text border-left border-right-0 border-top-0 border-bottom-0 bg-transparent">@</div>
            </div>
          </div>
        </form> */}
        
        <div className="btn-group btn-group-lg ml-auto">
          <button className="btn btn-outline-dark border-0 rounded-0" >
            <i className="fas fa-satellite-dish" />
          </button>
          <button className="btn btn-outline-dark border-0 rounded-0">
            <i className="fas fa-cloud-download-alt" />
          </button>
          <button className="btn btn-outline-dark border-0 rounded-0">
            <i className="fas fa-cogs" />
          </button>
          <div className="align-self-center">
            <div className="align-self-center px-2 font-cabin" style={{ fontSize: "18px" }}>
              <Clock format="hh:mm a" ticking={true} interval={5000} />
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    if (!this.state.isLoaded) {
      return (
        <div className="vw-100 vh-100 bg-dark d-flex justify-content-center align-items-center" onClick={() => {
          this.setState({ isLoaded: true });
        }}>
          <ReactLoading type="spinningBubbles" color="#ffffff" />
        </div>
      );
    } else {
      return (
        <div className="vw-100 vh-100 bg-dark d-flex flex-row font-cabin">
          <Sidebar visible={this.state.sidebar} companyName="Adleys" />
          <div className="d-flex flex-column h-100 w-100"
            style={{ backgroundColor: "#f5f3ed" }}
          >
            <this.topBar />
            <NavContext.Provider value={{ setNav: (navLink) => this.setState({ navback: navLink }) }}>
              <Switch>
                <Route path="/inventory">
                  <InventoryPage onNavChange={this.setNavBack} />
                </Route>
                <Route path="/vendor">
                  <VendorPage />
                </Route>
                <Route path="/backup">
                  <div className="p-4 w-100 h-100 d-flex align-items-center justify-content-center">
                    <div className="card">
                      <img src="https://i.imgflip.com/10bbo3.jpg" alt="bazinga" />
                    </div>
                  </div>
                </Route>

                {/* If no route is found */}
                <Route>
                  <Redirect to="/inventory" />
                </Route>
              </Switch>
            </NavContext.Provider>
          </div>
          {/* <this.navBar /> */}
        </div>
      );
    }
  }
}

export default App;