import React, { Component, CSSProperties } from 'react';
import { NavLink } from 'react-router-dom';
import './sidebar.css';

interface IProps {
  visible?: boolean,
  onToggle?: (e: any) => void,
  companyName: string
};

interface IState {
  visible: boolean
};

class Sidebar extends Component<IProps, IState> {
  rootRef = React.createRef<HTMLDivElement>();

  buttonStyle: CSSProperties = {

  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      visible: true
    };
  };

  close = () => {
    this.setState({ visible: false });
  };

  open = () => {
    this.setState({ visible: true });
    const root = this.rootRef.current;
    if (root)
      root.focus();
  };

  render() {
    const { visible } = this.state;
    return (
      <div className="position-relative d-flex flex-row" style={{ width: "20%", minWidth: "100px" }}>
        <div
          className="shadow"
          style={{
            background: "rgba(0, 0, 0, 0.1)",
            position: "absolute",
            top: '70px',
            left: 0,
            zIndex: 3,
            fontSize: "24px",
            cursor: "pointer",
            borderRadius: "0 30px 30px 0",
            marginLeft: !visible ? "0" : "-100%",
            transition: "margin 1.2s, background 500ms"
          }}
          onClick={this.open}
          onMouseEnter={(e) => e.currentTarget.style.background = "black"}
          onMouseLeave={(e) => e.currentTarget.style.background = "rgba(0, 0, 0, 0.1)"}
        >
          <div className="pl-2 pr-3 py-1">
            <i className="fas fa-bars" />
          </div>
        </div>
        <div ref={this.rootRef} className="bg-moss w-100 vh-100 shadow position-relative d-flex flex-column sb-font-scada">
          <div className="w-100 py-2 px-2 text-dark d-flex flex-row" style={{ borderBottom: "1px solid #1f1f1f" }}>
            <img className="" src={require("../resources/store_pro.png")} height="80" width="80" alt="logo" />
            <p className="flex-grow-1 my-auto h4 text-center py-1 text-uppercase sb-font-mansalva font-weight-bold" style={{ color: "orange", textShadow: "1px 1px 5px black" }}>
              {this.props.companyName!}
            </p>
          </div>
          <div className="w-100 py-2 px-1 font-weight-bold text-white" style={{ borderBottom: "1px solid #1f1f1f" }}>
            <p className="my-auto text-muted text-uppercase">Welcome</p>
            <big className="font-cabin" style={{ color: "orange" }}>Simanto R.</big>
          </div>
          <div className="btn-group btn-group-vertical w-100">
            <NavLink to="/inventory" className="btn btn-dark border-0 m-0 p-2 d-flex w-100 rounded-0 pointer">
              <div className="px-2 text-right text-center" style={{ width: "20%" }}><i className="fas fa-warehouse" /></div>
              <b className="text-left w-100">Inventory</b>
            </NavLink>
            <NavLink to='/vendor' className="btn btn-dark border-0 m-0 p-2 d-flex w-100 rounded-0 pointer">
              <div className="px-2 text-right text-center" style={{ width: "20%" }}><i className="fas fa-server" /></div>
              <b className="text-left w-100">Vendor</b>
            </NavLink>
            <NavLink to='/backup' className="btn btn-dark border-0 m-0 p-2 d-flex w-100 rounded-0 pointer">
              <div className="px-2 text-right text-center" style={{ width: "20%" }}><i className="fas fa-server" /></div>
              <b className="text-left w-100">Backup</b>
            </NavLink>
          </div>
          <div className="mt-auto mx-auto w-100">
            <div className="w-100 text-center">
              <small className="text-monospace text-muted">v0.0.3(alpha)</small>
            </div>
            <div className="w-100 text-center" style={{ background: "linear-gradient(180deg, #323339, #1f2024)" }}>
              <div className="btn-group py-2">
                <button className="btn btn-dark border-0 bg-transparent no-outline">
                  <span className="px-2"><i className="fab fa-react" /></span>
                </button>
                <button className="btn btn-dark border-0 bg-transparent no-outline">
                  <span className="px-2"><i className="fas fa-cogs" /></span>
                </button>
                <button className="btn btn-dark border-0 bg-transparent no-outline"
                  onClick={() => {
                    const electron: any = require('electron');
                    if (electron)
                      electron.remote.getCurrentWindow().close()
                  }}
                >
                  <span className="px-2"><i className="fas fa-sign-out-alt" /></span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Sidebar;