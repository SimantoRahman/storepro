import React, { Component, CSSProperties, MouseEvent } from 'react';
import ReactTable, { Column } from 'react-table';
import { Scrollbars } from 'react-custom-scrollbars';
import { toExcel } from '../../libraries/FileFormats';
import 'xlsx';
import 'datejs';
import 'react-table/react-table.css';

interface Props {
  onNavChange: (navLink?: string) => void;
  onClose?: () => void;
}

interface StateProps {
  items: Array<Item>;
  showingItem?: Item;
  page: number;
  pageLimit: number;
}

interface Item {
  sku: number,
  name: string,
  quantity: number,
  unitCost: number
}

export default class ViewInventory extends Component<Props, StateProps> {
  constructor(props: Props) {
    super(props);
    this.state = {
      items: [{
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Powarade",
        quantity: 26,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Gatorade",
        quantity: 19,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Coke 2L",
        quantity: 6,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Pepsi 512ML",
        quantity: 14,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "GingerAle 2L",
        quantity: 4,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Powarade",
        quantity: 26,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Gatorade",
        quantity: 19,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Coke 2L",
        quantity: 6,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Pepsi 512ML",
        quantity: 14,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "GingerAle 2L",
        quantity: 4,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Powarade",
        quantity: 26,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Gatorade",
        quantity: 19,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Coke 2L",
        quantity: 6,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Pepsi 512ML",
        quantity: 14,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "GingerAle 2L",
        quantity: 4,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Powarade",
        quantity: 26,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Gatorade",
        quantity: 19,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Coke 2L",
        quantity: 6,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "Pepsi 512ML",
        quantity: 14,
        unitCost: (Math.random() * 49) + 1
      }, {
        sku: Math.floor((Math.random() * 90000000) + 10000000),
        name: "GingerAle 2L",
        quantity: 4,
        unitCost: (Math.random() * 49) + 1
      }],
      showingItem: undefined,
      page: 0,
      pageLimit: 1
    };
  };

  componentDidMount() {
    // this.props.onNavChange!("/inventory/order");
    let divBody = document.querySelector('#viewinventory_root') as HTMLDivElement;
    // divBody.style.background = 'rgba(0, 0, 0, 0.7)';
    setTimeout(() => divBody.style.background = 'rgba(0, 0, 0, 0.7)', 50);
  }

  componentWillUnmount() {
    this.props.onNavChange!(undefined);
  }

  //#region Functions
  promptSave = () => {
    let electron = require('electron');

    // if on a browser
    if (!electron) {
      return;
    }

    const remote = electron.remote;
    const dialog = remote.dialog;
    const WIN = remote.getCurrentWindow();

    let options = {
      //Placeholder 1
      title: "Save file",

      //Placeholder 2
      defaultPath: `file.xlsx`,

      //Placeholder 4
      buttonLabel: "Save",

      //Placeholder 3
      filters: [
        { name: 'Spreadsheet', extensions: ['xlsx', 'csv'] }
      ]
    }

    // asynchronous - using callback
    dialog.showSaveDialog(WIN, options).then(({ filePath }: any) => {
      console.log(filePath);

      if (!filePath)
        return;

      try {
        toExcel(filePath, this.state.items);
        // this.saveToExcel(filePath, this.state.items);
      } catch (err) {
        console.log(err);
      }
    })
  }

  // filter method for react-table
  rangeFilter = (
    filter: { id: any, value: string | number },
    row: Array<number>,
    col: any[]
  ) => {
    const i = filter.id;
    const query: string = filter.value as string;
    if (query.includes('-')) {
      if (query.endsWith('-')) {
        return row[i] !== undefined && row[i] >= parseFloat(query);
      }
      const range = query.split('-');
      if (range.length === 2) {
        return row[i] !== undefined && row[i] <= parseFloat(range[1]) && row[i] >= parseFloat(range[0]) ? true : false;
      }
    }
    return true;
  }

  fetchItemCard = async (e: MouseEvent, index: number) => {
    const item = this.state.items[index];
    this.setState({ showingItem: item });
    return;
    // let response = await fetch(
    //   `https://api.storepro.app/{companyName}/{storeId}/inventory/get/${sku}`,
    //   { headers: [["Authorization", "BEARER <Token>"]] }
    // );

    // if (response.status !== 200)
    //   return

    // let json: { name: string, } = await response.json();
    // this.setState({ currentView: json });
  }

  onClose = (e: any) => {
    let divBody = document.querySelector('#viewinventory_body') as HTMLDivElement;
    let divRoot = document.querySelector('#viewinventory_root') as HTMLDivElement;
    divRoot.style.background = '';
    divBody.classList.replace('fadeIn', 'fadeOut');
    setTimeout(() => {
      this.props.onClose!();
    }, 700);
  }

  hoverCard = () => {
    let showingItem = this.state.showingItem as Item;

    return (
      <div className="position-absolute w-100 h-100 d-flex justify-content-center align-items-center animated fadeIn faster"
        style={{ backgroundColor: "rgba(0, 0, 0, 0.7)", zIndex: 100 }}
      >
        <div className="card w-50 h-50 bg-white font-cabin" style={{ boxShadow: "0 0 20px #303030" }}>
          <div className="card-body d-flex">
            <div className="list-group list-group-flush">
              <h4>{showingItem.name}</h4>
              <span className="list-group-item">Volume: {showingItem.quantity}ml</span>
              <span className="list-group-item">Unit Price: ${showingItem.unitCost.toFixed(2)}</span>
            </div>
            <div className="d-flex flex-column mx-auto justify-content-center align-items-center">
              <h3>In Stock</h3>
              <b className="text-info" style={{ fontSize: "64px", textShadow: "2px 2px 5px gray" }}>{showingItem.quantity}</b>
            </div>
          </div>
          <div className="card-footer text-center">
            <button className="btn btn-danger font-cabin text-uppercase" onClick={() => this.setState({ showingItem: undefined })}>Close</button>
          </div>
        </div>
      </div>
    );
  }
  //#endregion

  render() {
    const { showingItem, page, pageLimit } = this.state;

    // sort and store list
    let items = this.state.items.sort(function (a, b) {
      return a.unitCost - b.unitCost;
    });

    // calculate pages
    const itemsInAPage = 10;
    const _pageLimit = (items.length / 10);

    const filterComponent = ({ filter, onChange }: any) => (
      <div className="input-group selectable">
        <input
          className="form-control rounded-pill px-3 border border-blue"
          type="search"
          style={{ fontSize: '16px' }}
          onChange={event => onChange(event.target.value)}
        />
      </div>
    )

    const columns: Column<Item>[] = [
      {
        Header: () => <b>SKU <i className="fas fa-barcode" /></b>,
        accessor: "sku",
        Cell: ({ value, index }) => (
          <div className="text-center">
            <span className="btn btn-link p-0 font-weight-bold selectable"
              style={{ fontFamily: "Andale Mono", cursor: "pointer" }}
              onClick={(e) => this.fetchItemCard(e, index)}>
              {value}
            </span>
          </div>
        ),
        Filter: filterComponent,
        filterMethod: (filter: { id: number | string, value: string | number }, row: Array<string>, col: any[]) => {
          const query: string = filter.value as string;
          const i = filter.id as number;
          if (row[i].toLowerCase().includes(query.toLowerCase()))
            return true;
          else return false;
        }
      },
      {
        Header: "Name",
        headerStyle: { fontWeight: "bold" } as CSSProperties,
        accessor: "name",
        filterMethod: (filter: { id: string | number, value: string }, row: string[], col: any) => {
          // index number of the a row
          const i = filter.id as number;
          // search query
          const query = filter.value as string;
          // regular search
          if (row[i].toLocaleLowerCase().includes(query.toLocaleLowerCase()))
            return true;

          // default return
          return false;
        },
        Filter: filterComponent,
        Cell: ({ value }) => <div className="text-center my-0 align-self-center">{value}</div>
      },
      {
        Header: "Quantity",
        headerStyle: { fontWeight: "bold" } as CSSProperties,
        accessor: "quantity",
        filterMethod: this.rangeFilter,
        Filter: filterComponent,
        Cell: ({ value }) => <div className="text-center">{value < 10 && "0"}{value}</div>
      },
      {
        Header: () => <b><i className="fas fa-dollar-sign" /> Cost &#40;CAD&#41;</b>,
        headerStyle: { fontWeight: "bold" } as CSSProperties,
        accessor: "unitCost",
        filterMethod: this.rangeFilter,
        Filter: filterComponent,
        Cell: ({ value }) => <div className="text-center font-weight-bold big">{value.toFixed(2)}</div>
      }
    ]

    return (
      <div id="viewinventory_root" className="h-100 w-100 text-dark" style={{ fontFamily: "Calibri", fontSize: "20px", background: 'transparent', transition: 'background 0.4s', zIndex: 1 }}>
        {showingItem && <this.hoverCard />}
        <Scrollbars className="w-100 h-100" style={{ width: "100%", height: "100%" }}>
          <div id="viewinventory_body" className="w-100 h-100 p-4 d-flex flex-column align-items-center justify-content-center animated fadeIn faster shadow">
            <div className="w-100 bg-light">
              <div className="w-100 d-flexborder border-bottom-0">
                <div className="btn-group btn-group-square p-2 mr-auto">
                  <button className="btn btn-toolkit border-0 mr-1 selectable" onClick={this.onClose}>
                    <i className="fas fa-times-circle" />
                  </button>
                  <button className="btn btn-toolkit selectable">
                    <i className="fas fa-edit" />
                  </button>
                  <button className="btn btn-toolkit" onClick={this.promptSave}>
                    <i className="fas fa-save" />
                  </button>
                </div>
              </div>
            </div>
            <ReactTable className="bg-white w-100 -highlight -striped"
              filterable={true}
              loading={items === []}
              data={items}
              columns={columns}
              page={page}
              pages={_pageLimit}
              pageSize={itemsInAPage}
              showPageJump={true}
              defaultPageSize={items.length <= 10 ? items.length : 10}
              showPageSizeOptions={false}
              sortable={true}
              multiSort={true}
              PaginationComponent={() => {
                let nextDisabled = page == _pageLimit - 1;
                let prevDisabled = page <= 0;
                return (
                  <div className="d-flex justify-content-around border-top py-2">
                    <button className={`btn ${prevDisabled ? 'btn-secondary' : 'btn-primary'} selectable `}
                      onClick={() => {
                        let prevPage = page - 1;

                        if (prevPage >= 0)
                          this.setState({ page: prevPage });
                      }}
                    >
                      <i className="fas fa-caret-square-left" />
                    </button>
                    <div className="text-center align-self-center">Page {page + 1} of {_pageLimit}</div>
                    <button className={`btn ${nextDisabled ? 'btn-secondary' : 'btn-primary'} selectable`}
                      onClick={() => {
                        let nextPage = page + 1;

                        if (nextPage <= _pageLimit - 1)
                          this.setState({ page: nextPage });
                      }}
                    >
                      <i className="fas fa-caret-square-right" />
                    </button>
                  </div>
                )
              }}
            />
          </div>
        </Scrollbars>
      </div>
    );
  }
}