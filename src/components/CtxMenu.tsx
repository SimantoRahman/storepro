import React, { CSSProperties } from 'react';
import './menu.css';

interface Props {
  className?: string
  items: Array<IItem>,
  fontSize?: string,
  closeOnClick?: boolean
  rootElement?: () => JSX.Element
  style?: CSSProperties
}

interface States {
  visible: boolean
}

export interface IItem {
  label: string,
  onClick?: (e?: any) => void,
  icon?: string
}

/** TODO
 * Allow custom root component type
 * instead of DIV
 */

export default class CtxMenu extends React.Component<Props, States> {
  rootRef = React.createRef<HTMLDivElement>();
  menuRef = React.createRef<HTMLDivElement>();

  constructor(props: Props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  componentDidMount() {
    let root = this.rootRef.current!;
    this.menuRef.current!.style.visibility = "hidden";
    root.addEventListener('contextmenu', this.openMenu);
    root.addEventListener('focusout', this.closeMenu);
  }

  openMenu = (event: any) => {
    event.preventDefault();

    const xOffset = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft) + 5;
    const yOffset = Math.max(document.documentElement.scrollTop, document.body.scrollTop) + 5;

    const _menu = this.menuRef.current!;

    // set position to reflect mouse click position
    _menu.style.left = `${event.clientX + xOffset}px`;
    _menu.style.top = `${event.clientY + yOffset}px`;
    _menu.style.visibility = "visible";

    _menu.focus();
  }

  closeMenu = () => {
    const _menu = this.menuRef.current!;
    _menu.style.visibility = "hidden";
  }

  getItems() {
    const { items, closeOnClick } = this.props;
    if (closeOnClick) {
      return items.map(item => ({
        ...item,
        onClick: () => {
          this.closeMenu();
          if (item.onClick)
            item.onClick();
        },
      }))
    } else {
      return items;
    }
  }

  Option = ({ item }: { item: IItem }) => {
    return (
      <span
        className="item text-left font-cabin p-1"
        onClick={item.onClick}
        style={{ cursor: "pointer", fontSize: "16px" }}
      >
        {item.icon && (
          <span className="px-2"><i className={item.icon} /></span>
        )}
        {item.label}
      </span>
    )
  }

  render() {
    return (
      <div ref={this.rootRef} className={this.props.className} style={this.props.style!}>
        {this.props.children}
        {/* Menu goes here */}
        <div
          tabIndex={1}
          ref={this.menuRef}
          className="menu position-fixed"
        >
          {this.getItems().map((item, i) => {
            return <this.Option item={item} key={i} />
          })}
        </div>
      </div>
    )
  }
}