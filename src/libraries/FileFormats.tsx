import { WriteStream, createWriteStream } from 'fs';
import Excel from 'exceljs';

interface Item {
  sku: number,
  name: string,
  quantity: number,
  unitCost: number
}

export function toExcel(filepath: string, data: Item[], closeOnComplete = true) {
  let workbook = new Excel.Workbook();
  workbook.creator = 'StorePro';
  workbook.lastModifiedBy = 'Her';
  workbook.created = Date.today();
  workbook.lastPrinted = new Date(2016, 9, 27);

  workbook.views = [{
    x: 0, y: 0, width: 10000, height: 20000,
    firstSheet: 0, activeTab: 1, visibility: 'visible'
  }];

  let mainWorksheet = workbook.addWorksheet(
    'Datasheet',
    { pageSetup: { paperSize: 9, orientation: 'landscape' } }
  );

  mainWorksheet.addRow(['SKU', 'NAME', 'QUANTITY', 'COST']).commit();
  mainWorksheet.getRow(0).font = { bold: true };

  data.map(({ sku, name, quantity, unitCost }) => {
    mainWorksheet.addRow([sku, name, quantity, unitCost.toFixed(2)]).commit();
  });

  try {
    mainWorksheet.commit();
  } catch (err) {
    console.log(err);
  }

  const writeStream: WriteStream = createWriteStream(filepath);

  workbook.xlsx.write(writeStream).then(function () {
    if (closeOnComplete)
      writeStream.close();
  });
}