const HtmlWebpackPlugin = require('html-webpack-plugin');
// const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = [{
    mode: 'development',
    entry: './public/electron.js',
    target: 'electron-main',
    module: {
        rules: [{
            test: /\.ts$/,
            include: /src/,
            loader: 'ts-loader',
            options: {
                configFile: 'tsconfig.webpack.json'
            }
        }]
    },
    output: {
        path: __dirname + '/dist',
        filename: 'electron.js'
    }
}, {
    mode: 'development',
    entry: __dirname + '/src/index.tsx',
    target: 'electron-renderer',
    // devtool: 'source-map',
    module: {
        rules: [{
            test: /\.ts(x?)$/,
            include: /src/,
            loader: 'ts-loader',
            options: {
                configFile: 'tsconfig.webpack.json'
            }
        }, {
            test: /\.css$/,
            exclude: /node_modules/,
            use: ['style-loader', 'css-loader']
        }, {
            test: /\.css$/,
            include: /node_modules/,
            use: ['style-loader', 'css-loader']
        }, {
            test: /\.(jpe?g|jpg|png|gif|svg)$/i,
            use: [
                'file-loader?name=images/[name].[ext]',
                'image-webpack-loader?bypassOnDebug'
            ]
        }]
    },
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx']
    },
    plugins: [
        // new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: __dirname + '/public/index.html',
        })
    ]
}];